/*
  Libela, an event-loop abstraction library.

  This file is part of FOILS, the Freebox Open Interface Libraries.
  This file is distributed under a 2-clause BSD license, see
  LICENSE.TXT for details.

  Copyright (c) 2013, Devialet SAS
  See AUTHORS for details
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <android/looper.h>

#include <ela/backend.h>
#include <ela/ela.h>
#include <ela/android.h>

struct android_ela_el
{
    const struct ela_el_backend* backend;
    struct ela_event_source* firstSource;
    ALooper* looper;
    int opts;
    volatile int shouldRun; // 1 Yes, 0 No
};

struct ela_event_source
{
    // Callback function in Android
    ALooper_callbackFunc callback;
    int inCallback; // 0 No , 1 Yes
    int shouldBeRemoved;
    int shouldBeFreed;

    // Pointer to next source
    struct ela_event_source* nxt;

    // Context
    ela_handler_func* func;
    void* priv;
    void* context;

    // File descriptor
    int fd; // Reference to the file descriptor
    int fdOnce;
    int events;

    // Timer
    int timeoutMillis; // 0 returns immediately,
                       // -1 waits indefinitely until an event appears
    int timeoutDelays;
};
void _android_source_free( struct ela_el*, struct ela_event_source* );
int LooperCallback( int fd, int events, void* data );

void wakeLooper( struct ela_el* context)
{
    struct android_ela_el* ctx = ( struct android_ela_el* ) context;
    ALooper_wake( ctx->looper );
}

int computeMinTimeout( struct ela_el* context )
{
    struct android_ela_el* ctx = ( struct android_ela_el* ) context;
    int minTimeout = -1;
    struct ela_event_source* src = ctx->firstSource;

    while( src != NULL)
    {	
        if( ( src->timeoutDelays != -1 ) &&
            ( ( minTimeout > src->timeoutDelays ) || minTimeout == -1 ) )
        {
            minTimeout = src->timeoutDelays;
        }
        src = src->nxt;
    }
    
    return minTimeout;
}

void checkTimeouts( struct ela_el* context, int delta )
{
    struct android_ela_el* ctx = ( struct android_ela_el* ) context;
    struct ela_event_source* src = ctx->firstSource;
    while( src != NULL )
    {	
        struct ela_event_source* currentSrc = src;

        // We need to store the next here as the current source may be freed
        // in the callback !
        src = src->nxt;

        // Apply the elapsed time
        currentSrc->timeoutDelays -= delta;

        // The timeout fires
        if( currentSrc->timeoutDelays <= 0 )
        {
            // Reset the timer duration
            currentSrc->timeoutDelays = currentSrc->timeoutMillis;

            // Call the callback indicating it is a timeout event
            ( currentSrc->func )( currentSrc,
                                  currentSrc->fd,
                                  ELA_EVENT_TIMEOUT,
                                  currentSrc->priv );
        }
    }
}

int LooperCallback( int fd, int events, void* data )
{
    struct ela_event_source* source = ( struct ela_event_source* ) data;

    // Flag "inCallback"
	source->inCallback = 1 ;
    
	if( events & ALOOPER_EVENT_INPUT )
    {
        ( source->func )( source,
                          source->fd,
                          ELA_EVENT_READABLE,
                          source->priv );
	}
	if( events & ALOOPER_EVENT_OUTPUT )
    {
        ( source->func )( source,
                          source->fd,
                          ELA_EVENT_WRITABLE,
                          source->priv );
	}

    // Unflag "inCallback"
	source->inCallback = 0 ;

    // Deal with removing or freeing a source as it might be requested
    // by a user during callback. No need to check for shouldBeFreed as
    // _android_source_free always set shouldBeRemoved to true.
    if( source->fdOnce || source->shouldBeRemoved )
    {
        if( source->shouldBeFreed )
        {
            // We set the fd to -1 because the user won't need it anymore:
            // he called for the release of the source. We use fd as a flag to
            // ensure that it is not explicitly removed with ALooper_removeFd.
            // The removal of the fd will be done by looper by returning 0.
            source->fd = -1;
            // Free the memory associated with the source.
            _android_source_free( source->context, source );
        }
		return 0;
	}
    else
    {
		return 1;
	}
}

ela_error_t _android_source_alloc( struct ela_el* ctx,
                                   ela_handler_func* func,
                                   void* priv,
                                   struct ela_event_source** ret )
{
    struct android_ela_el* context = ( struct android_ela_el* ) ctx ;
    struct ela_event_source* source =
            malloc( sizeof( struct ela_event_source ) );

    // Check for memory allocation errors
    if( ! source )
    {
        return 1;
    }

    // Initialize the source
	source->nxt = NULL;
	source->func = func ; 
	source->priv = priv;
	source->context = context;
    source->callback = &LooperCallback;
	source->inCallback = 0;
	source->shouldBeRemoved = 0;
    source->shouldBeFreed = 0;
	source->fd = -1;
	source->fdOnce = 0;
	source->events = 0;
	source->timeoutMillis = -1 ;
    source->timeoutDelays = 0 ;
    
    // Store the result in the client provided pointer
    *ret = source;

    return 0;
}

ela_error_t _android_remove( struct ela_el* ctx , struct ela_event_source* src )
 {
    int err = 1;
    struct android_ela_el* context = ( struct android_ela_el* ) ctx;

    struct ela_event_source* current;
    struct ela_event_source *prev;

    // Remove it from ALooper if it is a valid file descriptor and we are not
    // currently in the ALooper callback context
    if( src->inCallback == 0 && src->fd != -1 )
    {
        err = ALooper_removeFd( context->looper, src->fd );
    }
    else
    {
        // Flag it for removal, the callback return code will deal with the
        // actual removal of the source
        src->shouldBeRemoved = 1;
    }

    // Remove it from the timeout list right away
    current = context->firstSource;
    prev = NULL;
    while( current != NULL )
    {
        if( src == current )
        {
            if( prev )
            {
                // This is in the middle of the list
                prev->nxt = src->nxt;
            }
            else
            {
                // There is no prev: this is the first element in the list
                context->firstSource = src->nxt;
            }
            // Clear the next pointer of the source
            src->nxt = NULL;
            break;
        }
        // Continue to look for the src
        prev = current;
        current = current->nxt;
    }

    return ( err == 1 ) ? 0 : 1;
 }

void _android_source_free( struct ela_el* ctx, struct ela_event_source* src )
{
    _android_remove( ctx, src );
    if( src->inCallback )
    {
        src->shouldBeFreed = 1;
    }
    else
    {
        free( src );
    }
}

ela_error_t _android_set_fd( struct ela_el* context,
                             struct ela_event_source* src,
							 int fd,
							 uint32_t flags )
 {
	src->fdOnce = flags & ELA_EVENT_ONCE;
	src->fd = fd;
	if( flags & ELA_EVENT_READABLE )
    {
		src->events |= ALOOPER_EVENT_INPUT;
	}
	if( flags & ELA_EVENT_WRITABLE )
    {
		src->events |= ALOOPER_EVENT_OUTPUT;
	}
	return 0;
 }

ela_error_t _android_set_timeout( struct ela_el* context,
                                  struct ela_event_source *src,
                                  const struct timeval *tv,
                                  uint32_t flags )
{
	src->timeoutMillis = ( int ) ( ( 1000 * tv->tv_sec ) + ( tv->tv_usec / 1000 ) );
	src->timeoutDelays = src->timeoutMillis;
	return 0;
}
 
ela_error_t _android_add( struct ela_el* ctx, struct ela_event_source* src )
{	
    int errorAdd;
    struct android_ela_el* context = ( struct android_ela_el* ) ctx;
    if( src->fd == -1){
    }
    // First add the file descriptor to the looper if the source has one
    if( src->fd != -1 )
    {
        errorAdd = ALooper_addFd( context->looper,
                                  src->fd,
                                  ALOOPER_POLL_CALLBACK,
                                  src->events,
                                  src->callback,
                                  src );
        if( errorAdd != 1 )
        {
            return 1;
        }
    }

    // Add to the linked list for timers
	src->nxt = context->firstSource;
    context->firstSource = src;

    return 0;
}
 

 
 void _android_exit( struct ela_el* ctx )
 {
    struct android_ela_el* context = ( struct android_ela_el* ) ctx;
	context->shouldRun = 0;
 }
 
void _android_run( struct ela_el* ctx )
{ 
    int minTimeout;
	int delta;
	struct timeval beforePoll;
    struct timeval afterPoll;
    
    struct android_ela_el* context = ( struct android_ela_el* ) ctx;
	context->shouldRun = 1;
    
    while( 1 == context->shouldRun )
	{
        // Determine the maximum duration the poll should take
        minTimeout = computeMinTimeout( ctx );

        // Get pre-poll time
        gettimeofday( &beforePoll, NULL );
        
        // Poll the fds, blocking the current thread
        ALooper_pollOnce( minTimeout, NULL, NULL, NULL );
        
        // Get post-poll time
        gettimeofday( &afterPoll, NULL );
		delta = ( 1000 * afterPoll.tv_sec + afterPoll.tv_usec / 1000 ) - 
                ( 1000 * beforePoll.tv_sec + beforePoll.tv_usec / 1000 );
                
        // Check if timeout sources have fired
        checkTimeouts( ctx, delta );
	}
}	

void _android_close( struct ela_el* ctx )
{
    struct android_ela_el* context = ( struct android_ela_el* ) ctx;

    context->shouldRun = 0;

    while( context->firstSource != NULL )
    {
        _android_source_free( ctx, context->firstSource );
	}

    ALooper_release( context->looper );
}


struct ela_el* _android_create( void );
    
static const struct ela_el_backend android_backend = { &_android_source_alloc,
                                                &_android_source_free,
                                                &_android_set_fd,
                                                &_android_set_timeout,
                                                &_android_add,
                                                &_android_remove,
                                                &_android_exit,
                                                &_android_run,
                                                &_android_close,
                                                "Android",
                                                &_android_create };

					

struct ela_el* _android_create( void )
{
    struct android_ela_el* result = malloc( sizeof( struct android_ela_el ) );
    result->backend = &android_backend;
    result->opts = 0;
    result->looper = ALooper_prepare( result->opts );
    ALooper_acquire( result->looper );
    result->shouldRun = 0;
	result->firstSource = NULL;
	
    return ( struct ela_el* ) result;
}

struct ela_el* ela_android( void )
{
	return _android_create();
}
