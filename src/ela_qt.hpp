/*
  Libela, an event-loop abstraction library.

  This file is part of FOILS, the Freebox Open Interface Libraries.
  This file is distributed under a 2-clause BSD license, see
  LICENSE.TXT for details.

  Copyright (c) 2013, Devialet SA
  See AUTHORS for details
 */

/**
   @file
   @module {Backends}
   @short Qt backend private
 */

#ifndef ela_qt_hpp_
#define ela_qt_hpp_

extern "C" {
#include <ela/ela.h>
#include <ela/backend.h>
}

#include <QtCore/QSocketNotifier>

struct ela_event_source : public QObject
{
    Q_OBJECT

public:
    ela_event_source( ela_handler_func* func, void* priv );

    ela_error_t setFd( int fd, uint32_t flags );
    ela_error_t setTimeout( const timeval* tv, uint32_t flags );

    ela_error_t add( void );
    ela_error_t remove( void );

    void destroy( void );

private slots:
    void readEvent( int fd );
    void writeEvent( int fd );

protected:
    virtual void timerEvent( QTimerEvent* e );

private:
    // Callback context
    bool _valid;
    ela_handler_func* _func;
    void* _priv;
    // File descriptor
    int _fd;
    bool _fdOnce;
    QSocketNotifier* _readNotifier;
    QSocketNotifier* _writeNotifier;
    // Timeout
    bool _timeoutOnce;
    int _timerId;
    int _timeoutDelayMs;
};

#endif
