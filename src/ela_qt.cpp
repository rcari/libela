/*
  Libela, an event-loop abstraction library.

  This file is part of FOILS, the Freebox Open Interface Libraries.
  This file is distributed under a 2-clause BSD license, see
  LICENSE.TXT for details.

  Copyright (c) 2012, Devialet SAS
  See AUTHORS for details
 */


#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <ela/qt.hpp>
#include "ela_qt.hpp"

#include <QtCore/QCoreApplication>
#include <QtCore/QSocketNotifier>
#include <QtCore/QThread>

#ifdef WIN32
#   include <WinSock2.h>    // For struct timeval!
#endif

namespace
{
    enum qt_context_type
    {
        QtCoreApp,
        QtThread
    };

    struct qt_ela_el
    {
        const struct ela_el_backend* backend;

        qt_context_type type;
        void* qt_context;
    };

    class ThreadRunner : public QThread
    {
    public:
        static void Exec( QThread* t )
        {
            static_cast< ThreadRunner* >( t )->exec();
        }
    };
}

ela_event_source::ela_event_source( ela_handler_func* func, void* priv )
    : _valid( true )
    , _func( func )
    , _priv( priv )
    , _fd( -1 )
    , _fdOnce( false )
    , _readNotifier( NULL )
    , _writeNotifier( NULL )
    , _timeoutOnce( false )
    , _timeoutDelayMs( -1 )
    , _timerId( -1 )
{
}

ela_error_t ela_event_source::setFd( int fd, uint32_t flags )
{
    _fdOnce = flags & ELA_EVENT_ONCE;
    _fd = fd;

    if( _readNotifier )
    {
        _readNotifier->disconnect( this );
        _readNotifier->deleteLater();
        _readNotifier = NULL;
    }

    if( _writeNotifier )
    {
        _writeNotifier->disconnect( this );
        _writeNotifier->deleteLater();
        _writeNotifier = NULL;
    }

    if( flags & ELA_EVENT_READABLE )
    {
        _readNotifier = new QSocketNotifier( _fd,
                                             QSocketNotifier::Read,
                                             this );
        _readNotifier->setEnabled( false );
        connect( _readNotifier,
                 SIGNAL( activated( int ) ),
                 SLOT( readEvent( int ) ) );
    }

    if( flags & ELA_EVENT_WRITABLE )
    {
        _writeNotifier = new QSocketNotifier( _fd,
                                              QSocketNotifier::Write,
                                              this );
        _writeNotifier->setEnabled( false );
        connect( _writeNotifier,
                 SIGNAL( activated( int ) ),
                 SLOT( writeEvent( int ) ) );
    }

    return 0;
}

ela_error_t ela_event_source::setTimeout( const timeval* tv, uint32_t flags )
{
    _timeoutOnce = flags & ELA_EVENT_ONCE;
    _timeoutDelayMs = ( tv->tv_sec * 1000 ) + ( tv->tv_usec / 1000 );
    return 0;
}

void ela_event_source::readEvent( int fd )
{
    ( *_func )( this, _fd, ELA_EVENT_READABLE, _priv );

    if( ! _valid )
    {
        // The source has been deleted...
        return;
    }

    if( _fdOnce )
    {
        remove();
    }
    else if( -1 != _timerId )
    {
        // Reset the timeout
        killTimer( _timerId );
        _timerId = startTimer( _timeoutDelayMs );
    }
}

void ela_event_source::writeEvent( int fd )
{
    ( *_func )( this, _fd, ELA_EVENT_WRITABLE, _priv );

    if( ! _valid )
    {
        // The source has been deleted...
        return;
    }

    if( _fdOnce || _timeoutOnce )
    {
        remove();
    }
    else if( -1 != _timerId )
    {
        // Reset the timeout
        killTimer( _timerId );
        _timerId = startTimer( _timeoutDelayMs );
    }
}

void ela_event_source::timerEvent( QTimerEvent* e )
{
    ( *_func )( this, _fd, ELA_EVENT_TIMEOUT, _priv );

    if( ! _valid )
    {
        // The source has been deleted...
        return;
    }

    if( _timeoutOnce || _fdOnce )
    {
        remove();
    }
}

ela_error_t ela_event_source::add( void )
{
    if( _readNotifier )
    {
        _readNotifier->setEnabled( true );
    }

    if( _writeNotifier )
    {
        _writeNotifier->setEnabled( true );
    }

    if( _timeoutDelayMs != -1 )
    {
        _timerId = startTimer( _timeoutDelayMs );
    }

    return 0;
}

ela_error_t ela_event_source::remove( void )
{
    if( _readNotifier )
    {
        _readNotifier->disconnect( this );
        _readNotifier->setEnabled( false );
    }

    if( _writeNotifier )
    {
        _writeNotifier->disconnect( this );
        _writeNotifier->setEnabled( false );
    }

    if( _timerId != -1 )
    {
        killTimer( _timerId );
        _timerId = -1;
    }

    return 0;
}

void ela_event_source::destroy( void )
{
    remove();
    _valid = false;
    // Important to use deleteLater() in case users delete the source
    // in a callback...
    deleteLater();
}

ela_error_t _qt_source_alloc( ela_el* context,
                              ela_handler_func* func,
                              void* priv,
                              ela_event_source** ret )
{
    Q_UNUSED( context );
    ela_event_source* src = new ela_event_source( func, priv );
    *ret = src;
    return 0;
}

void _qt_source_free( ela_el* context, ela_event_source* src )
{
    Q_UNUSED( context );
    if( src )
    {
        src->destroy();
    }
}

ela_error_t _qt_set_fd( ela_el* context,
                        struct ela_event_source* src,
                        int fd,
                        uint32_t flags )
{
    Q_UNUSED( context );
    return src->setFd( fd, flags );
}

ela_error_t _qt_set_timeout( ela_el* context,
                             ela_event_source* src,
                             const timeval* tv,
                             uint32_t flags )
{
    Q_UNUSED( context );
    return src->setTimeout( tv, flags );
}

ela_error_t _qt_add( ela_el* context, ela_event_source* src )
{
    Q_UNUSED( context );
    return src->add();
}

ela_error_t _qt_remove( ela_el* context, ela_event_source* src )
{
    Q_UNUSED( context );
    return src->remove();
}

void _qt_exit( ela_el* context )
{
    qt_ela_el* ctx = reinterpret_cast< qt_ela_el* >( context );
    switch( ctx->type )
    {
    case QtCoreApp:
        static_cast< QCoreApplication* >( ctx->qt_context )->exit();
        break;
    case QtThread:
        static_cast< QThread* >( ctx->qt_context )->exit();
        break;
    }
}

void _qt_run( ela_el* context )
{
    qt_ela_el* ctx = reinterpret_cast< qt_ela_el* >( context );
    switch( ctx->type )
    {
    case QtCoreApp:
        static_cast< QCoreApplication* >( ctx->qt_context )->exec();
        break;
    case QtThread:
        ThreadRunner::Exec( static_cast< QThread* >( ctx->qt_context ) );
        break;
    }
}

void _qt_close( ela_el* context )
{
    qt_ela_el* ctx = reinterpret_cast< qt_ela_el* >( context );
    delete ctx;
}

ela_el* _qt_create( void );

namespace
{
    static const ela_el_backend qt_backend = { &_qt_source_alloc,
                                               &_qt_source_free,
                                               &_qt_set_fd,
                                               &_qt_set_timeout,
                                               &_qt_add,
                                               &_qt_remove,
                                               &_qt_exit,
                                               &_qt_run,
                                               &_qt_close,
                                               "Qt",
                                               &_qt_create };

    int registerQtBackend( void )
    {
        ela_register( &qt_backend );
        return 1;
    }

    // WARNING: the static registration does not work when the library
    // is compiled statically. None of this object code is called directly
    // thus the linker simply removes this compilation unit from the final
    // executable, yielding no static initialization of the module.
    const static int registered = registerQtBackend();
}

ela_el* _qt_create( void )
{
    if( ! qApp )
    {
        return NULL;
    }
    return ela_qt_app( qApp );
}

ela_el* ela_qt_app( QCoreApplication* app )
{
    qt_ela_el* result = new qt_ela_el;
    result->backend = &qt_backend;
    result->type = QtCoreApp;
    result->qt_context = app ? app : qApp;

    return reinterpret_cast< ela_el* >( result );
}

ela_el* ela_qt_thread( QThread* thread )
{
    qt_ela_el* result = new qt_ela_el;
    result->backend = &qt_backend;
    result->type = QtThread;
    result->qt_context = thread;

    return reinterpret_cast< ela_el* >( result );
}
