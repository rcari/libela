
#ifndef ela_android_h_
#define ela_android_h_

struct ela_el;

struct ela_el* ela_android();

// Public functions needed to use the library in a monothreaded app
void checkTimeouts( struct ela_el*, int );
int computeMinTimeout( struct ela_el* );
void wakeLooper( struct ela_el* );

#endif /* ela_android_h_ */
