/*
  Libela, an event-loop abstraction library.

  This file is part of FOILS, the Freebox Open Interface Libraries.
  This file is distributed under a 2-clause BSD license, see
  LICENSE.TXT for details.

  Copyright (c) 2013, Devialet SA
  See AUTHORS for details
 */

/**
   @file
   @module {Backends}
   @short Qt backend
 */


#ifndef ELA_QT_HPP
#define ELA_QT_HPP

extern "C" {
#include <ela/ela.h>
}

#include <QtCore/QCoreApplication>
#include <QtCore/QThread>

/**
   @this creates an adapter to a Qt core application

   @returns an event loop, or NULL if Qt support is
   unavailable.
 */
ela_el* ela_qt_app( QCoreApplication* app = NULL );

/**
   @this creates an adapter to a Qt QThread

   @returns an event loop, or NULL if Qt support is
   unavailable.
 */
ela_el* ela_qt_thread( QThread* thread );

#endif /* ELA_QT_HPP */
